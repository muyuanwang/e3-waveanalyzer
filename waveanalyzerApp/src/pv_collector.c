#include <string.h>    // Provides memcpy prototype
#include <stdlib.h>    // Provides calloc prototype
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <registryFunction.h>

#define MAXPOINTS 200000
#define F0 704420       // system frequency for TS2 in kHz
#define MAXSTEPS 250000 // 1 step ~ 1Hz

/*
 *   simply copy the input waveform record VAL
 *   into the output waveform
 *
 *   add the average filter and cavity gradient calibration for cavity field channel
 */
static int waveform_copy(aSubRecord *precord)
{
    // number of elements of the waveform
    unsigned long numPoints; 
    numPoints=precord->nea;
    if (numPoints>MAXPOINTS) {
        printf("[aSub ERROR] Number of elements of the waveform is too high\n");
        return -1;
    }
    // point to waveform values
    double *input_wave = (double*)precord->a;
    // cavity PU AI0 gradient calibration
    short eacc_enable = *(short*)precord->c;
    // cold cable attenuation
    double att = *(double*)precord->d;
    // cavity kt
    double kt  = *(double*)precord->e;
    // cavity PU in watts
    int m;
    double cavity_pu[numPoints]; 
    if (eacc_enable==0) {
        for (m=0; m<numPoints; m++) {
            if (isnan(input_wave[m]) || input_wave[m]<0) {input_wave[m]=0;}
            cavity_pu[m]=input_wave[m]*pow(10,att/10);
        }
    }
    else if (eacc_enable==1) {
        for (m=0; m<numPoints; m++) {
            if (isnan(input_wave[m]) || input_wave[m]<0) {input_wave[m]=0;}
            cavity_pu[m]=pow(input_wave[m]/kt,2);
        }
    }
    // average filter
    int i;
    double sum=0;
    for (i=0; i<numPoints; i++) {sum+=cavity_pu[i];}
    short avg = *(short*)precord->b;
    double output_wave[numPoints];
    if (avg==0) {
        for (i=0; i<numPoints; i++) {output_wave[i]=cavity_pu[i];}
    }
    else if (avg==1) {
        for (i=0; i<numPoints; i++) {output_wave[i]=cavity_pu[i]-sum/numPoints;}
    }
    memcpy(precord->vala,output_wave,numPoints*sizeof(double));
    precord->neva=numPoints;

    /* Done! */
    return 0;
}

/*
 * roi module calculation
 * 
 * Parameters:
 *
 *  INA: sampling frequency
 *  INB: near-IQ N from LLRF
 *  INC: fft frequency xaxis
 *  IND: roi start
 *  INE: roi size
 *  INF: stats index for maximum amplitude
 *
 * Output:
 *
 *  VALA: @param [out] [scalar] roi minX
 *  VALB: @param [out] [scalar] roi sizeX
 *  VALC: @param [out] [scalar] peak a.u frequency
 *  VALD: @param [out] [waveform] roi frequency
 *
 */
static int roi_calculation(aSubRecord *precord)
{
    // sampling frequency
    const double fsampling      = *(double*)precord->a;
    // Near-IQ N
    const long iqn              = *(long*)precord->b;
    // fft frequency xaxis
    const double *fft_frequency	=  (double*)precord->c;
    const int Nin               =  precord->nec;
    // fft roi start
    const double start          = *(double*)precord->d;
    // fft roi size
    const double size           = *(double*)precord->e;
    // stats maximum X
    const long max_index        = *(long*)precord->f;
 
    /* VALA: roi minX */
    int roi_minX;
    roi_minX = (int)ceil(iqn*2*start*Nin/fsampling/1000);
    *(long*)precord->vala=roi_minX;
    precord->neva=1;
    /* VALB: roi sizeX */
    int roi_sizeX;
    roi_sizeX = (int)ceil(iqn*2*size*Nin/fsampling/1000);
    *(long*)precord->valb=roi_sizeX;
    precord->nevb=1;
    /* VALD: roi frequency xaxis */
    int i;
    double roi_fft_frequency[roi_sizeX];
    for (i=0; i<roi_sizeX; i++) {roi_fft_frequency[i]=fft_frequency[i+roi_minX];}
    memcpy(precord->vald,roi_fft_frequency,roi_sizeX*sizeof(double));
    precord->nevd=roi_sizeX;
    /* VALC: peak a.u frequency */
    double peak_frequency;
    peak_frequency=roi_fft_frequency[max_index];
    *(double*)precord->valc=peak_frequency;
    precord->nevc=1;

    /* Done! */
    return 0;
}

/*
 * array operation
 * 
 * Parameters:
 *
 *  INA: input xaxis
 *  INB: input yaxis
 *  INC: system frequency
 *  IND: tuner motor position
 *  INE: roi peak frequency
 *  INF: tri-state control for array operation
 *
 * Output:
 *
 *  VALA: @param [out] [waveform] xaxis
 *  VALB: @param [out] [waveform] yaxis
 *
 */
static int array_operation(aSubRecord *precord)
{
    // system frequency
    const double frequency	= *(double*)precord->c;
    // tuner motor position readback
    const double steps		= *(double*)precord->d;
    // cavity frequency
    const double peak_frequency = *(double*)precord->e;
    // control for array operation
    const short ctrl            = *(short *)precord->f;
    // number of data points in INA 
    const int numPoints=precord->nea;
    // INA and INB need to have the same number of points
    if (numPoints==0 || precord->neb!=numPoints) {
        printf("[aSub ERROR] invalid input array!\n");
        return -1;
    }
    // input waveform for xaxis and yaxis
    double *input_xaxis = (double*)precord->a;
    double *input_yaxis = (double*)precord->b;

    int i;
    // add
    if (ctrl==0) {
        if (numPoints==1 && input_xaxis[0]==0 && input_yaxis[0]==0) {
            double xaxis[1]={steps};
            double yaxis[1]={1000*frequency-peak_frequency};
            printf("[aSub Initial] motor steps: %.lf, @f: %.1lfkHz\n",steps,1000*frequency-peak_frequency);
            memcpy(precord->vala,xaxis,sizeof(double));
            precord->neva=1;
            memcpy(precord->valb,yaxis,sizeof(double));
            precord->nevb=1;
        }
        else {
            double xaxis[numPoints+1],yaxis[numPoints+1];
            for (i=0; i<numPoints; i++) {
                xaxis[i]=input_xaxis[i];
                yaxis[i]=input_yaxis[i];
            }
            xaxis[numPoints]=steps;
            yaxis[numPoints]=1000*frequency-peak_frequency;
            printf("[aSub Add] motor steps: %.lf, @f: %.1lfkHz\n",steps,1000*frequency-peak_frequency);
            memcpy(precord->vala,xaxis,(numPoints+1)*sizeof(double));
            precord->neva=numPoints+1;
            memcpy(precord->valb,yaxis,(numPoints+1)*sizeof(double));
            precord->nevb=numPoints+1;
        }
    }
    // remove
    else if (ctrl==1) {
        if (numPoints==1) {
            printf("[aSub Stop] last point!\n");
            return -1;
        }
        else {
            double xaxis[numPoints-1],yaxis[numPoints-1];
            for (i=0; i<numPoints-1; i++) {
                xaxis[i]=input_xaxis[i];
                yaxis[i]=input_yaxis[i];
            }
            printf("[aSub Remove] motor steps: %.lf, @f: %.1lfkHz\n",steps,1000*frequency-peak_frequency);
            memcpy(precord->vala,xaxis,(numPoints-1)*sizeof(double));
            precord->neva=numPoints-1;
            memcpy(precord->valb,yaxis,(numPoints-1)*sizeof(double));
            precord->nevb=numPoints-1;
        }
    }
    // clear array
    else if (ctrl==2) {
        double xaxis[1]={0.0};
        double yaxis[1]={0.0};
        printf("[aSub Clear]\n");
        memcpy(precord->vala,xaxis,sizeof(double));
        precord->neva=1;
        memcpy(precord->valb,yaxis,sizeof(double));
        precord->nevb=1;
    }
    
    /* Done! */
    return 0;
}

/**
 *
 * @brief: calculate simple linear regression y(x)=k*x+b
 *
 *      double *steps, 
        double *freq, 
        size_t Nsamples,
   	double *intercept, double *slope
 * @param [in]  steps		input data steps array
 * @param [in]  freq		input data frequency array
 * @param [in]  Nsample		size of data array
 * @param [out] intercept	intercept b
 * @param [out] slope		slope k
 *
 *
 * fits a linear function y(x)=kx+b to the input data,
 * sampled with dt spacing, using standard linear regression
 *
 **/
void linear_regression(
        double *steps, double *freq,
        size_t Nsamples,
        double *intercept, double *slope)
{
    // 1. sum
    int i;
    double sumx=0, sumy=0;
    for(i=0; i<Nsamples; i++) {
        sumx += steps[i];
        sumy += freq[i];
    }
    // 2. average
    double x_avg=sumx/Nsamples;
    double y_avg=sumy/Nsamples;
    /*
     * 3. sums for ... 
     *	3.1 denominator: sum (x_i - x_avg)*(y_i - y_avg)
     *	3.2 numerator:	 sum (x_i - x_avg)^2
     */
    int j;
    double denom=0, num=0;
    for (j=0; j<Nsamples; j++) {
        denom  += (steps[j]-x_avg)*(freq[j]-y_avg);
        num    += pow((steps[j]-x_avg),2);
    }
    *slope = denom/num;
    *intercept = y_avg-(*slope)*x_avg;
}

/*
 * cavity tuning sensitivity calculation
 *
 * Parameters:
 *
 *  INA: xaxis
 *  INB: yaxis
 *  INC: xmin (minimum for window)
 *  IND: xmax (maximum for window)
 *
 * Output:
 *
 *  VALA: Df/DN
 *
 */
static int DfDN_calculation(aSubRecord *precord)
{
    // number of data points in INA
    const int Nin=precord->nea;
    // INA and INB need to have the same number of points
    if (precord->neb!=Nin) {
        printf("[aSub ERROR] invalid array size!\n");
        return -1;
    }

    // input waveform for xaxis and yaxis
    double *xaxis = (double*)precord->a;
    double *yaxis = (double*)precord->b;
    // range for window
    const long xmin = *(long *)precord->c;
    const long xmax = *(long *)precord->d;
    if (xmax<=xmin) {
        printf("[aSub ERROR] invalid window parameters!\n");
        return -1;
    }
    int i;
    int i_start=0, i_end=0;
    for (i=0; i<Nin; i++) {
        if (xmin==0) {i_start=0;}
        if (xmax>=MAXSTEPS) {i_end=Nin-1;}
        else if (xmin!=0 && xaxis[i]<=xmin && xaxis[i+1]>xmin) {i_start=i+1;}
        else if (xaxis[i]<=xmax && xaxis[i+1]>xmax) {i_end=i;}
    }
    const int length=i_end-i_start+1;
    double input_xaxis[length], input_yaxis[length];
    for (i=0; i<length; i++) {
        input_xaxis[i] = xaxis[i+i_start];
        input_yaxis[i] = yaxis[i+i_start];
    }
    // calculation by linear regression
    double intercept, slope;
    linear_regression(input_xaxis, input_yaxis, length, &intercept, &slope);

    /* VALA: cavity tuning sensitivity */
    /* Note: 1 screw turn ~ 20000 full steps;  /step ~ /Hz; */
    double DfDN;
    DfDN=slope*20000;
    printf("cavity tuning sensitivity: %.3lf kHz/screw turns\n",DfDN);
    *(double*)precord->vala=DfDN;
    precord->neva=1;

    /* Done! */
    return 0;
}
epicsRegisterFunction(waveform_copy);
epicsRegisterFunction(roi_calculation);
epicsRegisterFunction(array_operation);
epicsRegisterFunction(DfDN_calculation);
