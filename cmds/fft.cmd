require asyn
require adcore
require admisc
require nddriverstdarrays
require adsupport
require calc
require sequencer
require busy
require waveanalyzer
# require iocstats

epicsEnvSet("TOP",	"$(E3_CMD_TOP)/..")

#
#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

# The queue size for all plugins
epicsEnvSet("QSIZE",    "20")

# The maximim image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",    "40000")

# The maximim image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",    "1")

# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS",   "40000")

# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS",   "500")

# The number of elements in the driver waveform record
epicsEnvSet("NELEMENTS","40000")

# The datatype of the waveform record
epicsEnvSet("FTVL",     "DOUBLE")

# The asyn interface waveform record
epicsEnvSet("TYPE",     "Float64")

#asynSetMinTimerPeriod(0.001)

# Define CA maximum array bytes
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","10000000")

## IOC health monitor
#epicsEnvSet("IOCNAME",	"TS2-010RFC:SC-IOC-013")
#iocshLoad("$(iocstats_DIR)/iocStats.iocsh")

############################################################
# general
epicsEnvSet("P",	"TS2-010CRM:")
epicsEnvSet("R1",	"EMR-CAV-010:")
epicsEnvSet("R2",	"EMR-CAV-020:")
epicsEnvSet("R3",	"EMR-CAV-030:")
epicsEnvSet("R4",	"EMR-CAV-040:")

# digitiser and llrf
epicsEnvSet("PD",	"TS2-010RFC:")
epicsEnvSet("RD1",	"RFS-DIG-201:")
epicsEnvSet("RD2",	"RFS-DIG-202:")
epicsEnvSet("RD3",	"RFS-DIG-101:")
epicsEnvSet("RD4",	"RFS-DIG-102:")
epicsEnvSet("LLRF1",	"RFS-LLRF-201:")
epicsEnvSet("LLRF2",	"RFS-LLRF-202:")
epicsEnvSet("LLRF3",	"RFS-LLRF-101:")
epicsEnvSet("LLRF4",	"RFS-LLRF-102:")

# tuner motor
epicsEnvSet("MOTOR1",	"EMR-SM-010:")
epicsEnvSet("MOTOR2",	"EMR-SM-020:")
epicsEnvSet("MOTOR3",	"EMR-SM-030:")
epicsEnvSet("MOTOR4",	"EMR-SM-040:")

# plugin modules
epicsEnvSet("FFT",	"FFT1:")
epicsEnvSet("PLUGIN1",	"ROI1:")
epicsEnvSet("PLUGIN2",	"Stats1:")

# ndarray port name
epicsEnvSet("PORT1",	"NDSA1")
epicsEnvSet("PORT2",	"NDSA2")
epicsEnvSet("PORT3",	"NDSA3")
epicsEnvSet("PORT4",	"NDSA4")
#
#
###########################################################
#
# Cavity1 PU configuration 
#
epicsEnvSet("INP_WAVE_1", "$(PD)$(RD1)Dwn0-Cmp0")
epicsEnvSet("CH1",        "1")
iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh",	"P=$(P), R=$(R1), ROI=$(PLUGIN1), STATS=$(PLUGIN2), PORT=$(PORT1), XSIZE=$(XSIZE), QSIZE=$(QSIZE), NELEMENTS=$(NELEMENTS), NCHANS=$(NCHANS), FTVL=$(FTVL), TYPE=$(TYPE), CH=$(CH1), FFT=$(FFT), ADDR=0, NAME=A")
iocshLoad("$(waveanalyzer_DIR)connectPV.iocsh",		"P=$(P), R=$(R1), PD=$(PD), RD=$(RD1), FFT=$(FFT), LLRF=$(LLRF1), ROI=$(PLUGIN1), STATS=$(PLUGIN2), MOTOR=$(MOTOR1), NELEMENTS=$(NELEMENTS), INP_WAVE=$(INP_WAVE_1)")
iocshLoad("$(waveanalyzer_DIR)afterInitCommand.iocsh",	"P=$(P), R=$(R1), ROI=$(PLUGIN1), STATS=$(PLUGIN2), NELEMENTS=$(NELEMENTS), TYPE=$(TYPE), CH=$(CH1)"
#
#
###########################################################
#
# Cavity2 PU configuration 
#
epicsEnvSet("INP_WAVE_2", "$(PD)$(RD2)Dwn0-Cmp0")
epicsEnvSet("CH2",        "2")
iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh",	"P=$(P), R=$(R2), ROI=$(PLUGIN1), STATS=$(PLUGIN2), PORT=$(PORT2), XSIZE=$(XSIZE), QSIZE=$(QSIZE), NELEMENTS=$(NELEMENTS), NCHANS=$(NCHANS), FTVL=$(FTVL), TYPE=$(TYPE), CH=$(CH2), FFT=$(FFT), ADDR=0, NAME=B")
iocshLoad("$(waveanalyzer_DIR)connectPV.iocsh",		"P=$(P), R=$(R2), PD=$(PD), RD=$(RD2), FFT=$(FFT), LLRF=$(LLRF2), ROI=$(PLUGIN1), STATS=$(PLUGIN2), MOTOR=$(MOTOR2), NELEMENTS=$(NELEMENTS), INP_WAVE=$(INP_WAVE_2)")
iocshLoad("$(waveanalyzer_DIR)afterInitCommand.iocsh",	"P=$(P), R=$(R2), ROI=$(PLUGIN1), STATS=$(PLUGIN2), NELEMENTS=$(NELEMENTS), TYPE=$(TYPE), CH=$(CH2)"
#
#
###########################################################
#
# Cavity3 PU configuration 
#
epicsEnvSet("INP_WAVE_3", "$(PD)$(RD3)Dwn0-Cmp0")
epicsEnvSet("CH3",        "3")
iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh",	"P=$(P), R=$(R3), ROI=$(PLUGIN1), STATS=$(PLUGIN2), PORT=$(PORT3), XSIZE=$(XSIZE), QSIZE=$(QSIZE), NELEMENTS=$(NELEMENTS), NCHANS=$(NCHANS), FTVL=$(FTVL), TYPE=$(TYPE), CH=$(CH3), FFT=$(FFT), ADDR=0, NAME=C")
iocshLoad("$(waveanalyzer_DIR)connectPV.iocsh",		"P=$(P), R=$(R3), PD=$(PD), RD=$(RD3), FFT=$(FFT), LLRF=$(LLRF3), ROI=$(PLUGIN1), STATS=$(PLUGIN2), MOTOR=$(MOTOR3), NELEMENTS=$(NELEMENTS), INP_WAVE=$(INP_WAVE_3)")
iocshLoad("$(waveanalyzer_DIR)afterInitCommand.iocsh",	"P=$(P), R=$(R3), ROI=$(PLUGIN1), STATS=$(PLUGIN2), NELEMENTS=$(NELEMENTS), TYPE=$(TYPE), CH=$(CH3)"
#
#
###########################################################
#
# Cavity4 PU configuration 
#
epicsEnvSet("INP_WAVE_4", "$(PD)$(RD4)Dwn0-Cmp0")
epicsEnvSet("CH4",        "4")
iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh",	"P=$(P), R=$(R4), ROI=$(PLUGIN1), STATS=$(PLUGIN2), PORT=$(PORT4), XSIZE=$(XSIZE), QSIZE=$(QSIZE), NELEMENTS=$(NELEMENTS), NCHANS=$(NCHANS), FTVL=$(FTVL), TYPE=$(TYPE), CH=$(CH4), FFT=$(FFT), ADDR=0, NAME=D")
iocshLoad("$(waveanalyzer_DIR)connectPV.iocsh",		"P=$(P), R=$(R4), PD=$(PD), RD=$(RD4), FFT=$(FFT), LLRF=$(LLRF4), ROI=$(PLUGIN1), STATS=$(PLUGIN2), MOTOR=$(MOTOR4), NELEMENTS=$(NELEMENTS), INP_WAVE=$(INP_WAVE_4)")
iocshLoad("$(waveanalyzer_DIR)afterInitCommand.iocsh",	"P=$(P), R=$(R4), ROI=$(PLUGIN1), STATS=$(PLUGIN2), NELEMENTS=$(NELEMENTS), TYPE=$(TYPE), CH=$(CH4)"
#
#
###########################################################
#
# Call iocInit to start the IOC
#
iocInit()
